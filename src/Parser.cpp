#include <fstream>
#include <sstream>
#include <iostream>
#include "includes/Parser.h"

std::ifstream file;

void parsingData(Vector<std::string> *&MenuItems, Vector<std::string> *&MenuKeyMap, Vector<std::string> &Names, int &items) {
    std::string token;
    while (!file.eof()) {
        getline(file, token, '{');
        getline(file, token, '}');
        items++;
    }
    MenuItems = new Vector<std::string>[items];
    MenuKeyMap = new Vector<std::string>[items];
    file.clear();
    file.seekg(0);
    for (int size = 0; !file.eof(); size++) {
        getline(file, token, '"');
        getline(file, token, '"');
        Names.push_back(token + "(F" + std::to_string(size + 1) + ")");
        getline(file, token, '{');
        getline(file, token, '}');
        int count = -1;
        for (int i = 0; i < token.length(); i++)
            if (token[i] == '\n')
                count++;
        std::istringstream iterToken(token);
        for (int i = 0; i < count; i++) {
            getline(iterToken, token, '\n');
            getline(iterToken, token, '"');
            getline(iterToken, token, '"');
            MenuItems[size].push_back(token);
            getline(iterToken, token, ':');
            getline(iterToken, token, '"');
            getline(iterToken, token, '"');
            MenuKeyMap[size].push_back(token);
            getline(iterToken, token, ',');
        }
        iterToken.clear();
        getline(file, token, '\n');
    }
    file.close();
};

void readData(Vector<std::string> *&MenuItems, Vector<std::string> *&MenuKeyMap, Vector<std::string> &Names, int &items) {
    file.open("../src/MenuItems.txt");
    if (!file) {
        std::cerr << "ERROR: DATA FILE NOT FOUND" << std::endl;
        exit(-1);
    }
    parsingData(MenuItems, MenuKeyMap, Names, items);
}