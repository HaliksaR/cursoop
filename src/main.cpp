#include <sys/ioctl.h>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstring>

#include "includes/Window.h"
#include "includes/Menu.h"
#include "includes/Parser.h"
#include "includes/Vector.h"

using namespace std;

int main() {
    Vector<string> *MenuItems;
    Vector<string> *MenuKeyMap;
    Vector<string> Names;
    int items = 0, length = 0;
    readData(MenuItems, MenuKeyMap, Names, items);

    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

    Window Map(w.ws_row, w.ws_col, 0, 0);
    Map.create_new_win();
    string Namesstr;
    for (int j = 0; j < Names.size(); ++j)
        Namesstr += Names[j] + " ";
    Map.init_border(Namesstr, "Esc");
    Menu *MenuArray[items];
    length = (int)(0 - Names[0].length());
    for (int i = 0; i < items; i++)
        MenuArray[i] = new Menu(MenuItems[i], MenuKeyMap[i], 10, 40, 2, length += Names[i].length() + 1);
    Map.window_key_events();
    int k = 0;
    while (k != -1) {
        switch (Map.window_key_events()) {
            case 0:
                k = -1;
                break;
            case 1:
                MenuArray[0]->create_new_win();
                k = MenuArray[0]->menu_key_driver(KEY_F(1));
                break;
            case 2:
                MenuArray[1]->create_new_win();
                k = MenuArray[1]->menu_key_driver(KEY_F(2));
                break;
            case 3:
                MenuArray[2]->create_new_win();
                k = MenuArray[2]->menu_key_driver(KEY_F(3));
                break;
            case 4:
                MenuArray[3]->create_new_win();
                k = MenuArray[3]->menu_key_driver(KEY_F(4));
                break;
            case 5:
                MenuArray[4]->create_new_win();
                k = MenuArray[4]->menu_key_driver(KEY_F(5));
                break;
        }
    }
    endwin();
    return 0;
}

