#ifndef MAIN_PARSER_H
#define MAIN_PARSER_H

#include <string>
#include "Vector.h"

void readData(Vector<std::string> *&, Vector<std::string> *&, Vector<std::string> &, int &);

#endif //MAIN_PARSER_H
