#ifndef MAIN_VECTOR_H
#define MAIN_VECTOR_H

#include <unistd.h>

template<typename T>
class Vector {
private:
    T *values;
    size_t count;
    size_t reserved;
public:
    Vector(): values(NULL), count(0), reserved(0) {}
    ~Vector() {
        for (size_t i = 0; i < count; i++)
            (values + i)->~T();
        free(values);
    }
    Vector<T> &operator=(const Vector &argument) {
        if (&argument != this) {
            for (size_t i = 0; i < count; i++)
                (values + i)->~T(); // lifetime of values + i ends
            free(values);
            values = NULL;
            count = argument.count;
            reserved = argument.reserved;
            if (reserved > 0) {
                values = (T *) malloc(reserved * sizeof(T));
                for (size_t i = 0; i < count; i++)
                    new(values + i) T(argument.values[i]);
            }
        }
        return *this;
    }
    void push_back(const T &argument) {
        if (count == reserved) {
            T *oldValues = values;
            values = (T *) malloc((reserved + 1) * sizeof(T));
            for (size_t i = 0; i < count; i++) {
                new(values + i) T(oldValues[i]);
                (oldValues + i)->~T();
            }
            free(oldValues);
            reserved++;
        }
        new(values + count) T(argument);
        count++;
    }
    T &operator[](size_t i) { return values[i]; }
    size_t size() const { return count; }
    void clear() {
        for (size_t i = 0; i < count; ++i)
            (values+ i)->~T();
        count = 0;
    }
};

#endif //MAIN_VECTOR_H
