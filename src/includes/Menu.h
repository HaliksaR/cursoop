#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "Window.h"
#include <menu.h>
#include "Vector.h"


class Menu : public Window {

private:
    ITEM **my_items;
    int key;
    MENU *my_menu;

    Vector<std::string> items;
    Vector<std::string> itemsKey;
public:
    Menu(Vector<std::string> items, Vector<std::string> itemsKey, int height, int width, int starty,
         int startx) : Window(height, width, starty, startx) {
        this->items = items;
        this->itemsKey = itemsKey;
        init_menu("");
    }

    int menu_key_driver(int exit) {
        std::string num = "";
        for (int i = 0; i < 12; ++i)
            if (exit == KEY_F(i))
                num = "F" + std::to_string(i);
        init_menu(num);
        keypad(this->window, TRUE);
        while ((this->key = getch()) != exit) {
            switch (this->key) {
                case KEY_DOWN:
                    menu_driver(this->my_menu, REQ_DOWN_ITEM);
                    break;
                case KEY_UP:
                    menu_driver(this->my_menu, REQ_UP_ITEM);
                    break;
                case 10: {
                    if (func(item_name(current_item(this->my_menu))) == -1) {
                        return -1;
                    } else if (func(item_name(current_item(this->my_menu))) == 0) {
                        return 0;
                    }
                    pos_menu_cursor(this->my_menu);
                    break;
                }
            }
            wrefresh(this->window);
        }
    }

    ~Menu() {
        unpost_menu(this->my_menu);
        free_menu(this->my_menu);
        for (int i = 0; i < this->items.size(); ++i)
            free_item(this->my_items[i]);
        delwin(this->window);
    }

private:
    void init_menu(std::string numkey) {
        my_items = (ITEM **) calloc(this->items.size() + 1, sizeof(ITEM *));

        for (int i = 0; i < this->items.size(); i++)
            this->my_items[i] = new_item(this->items[i].c_str(), this->itemsKey[i].c_str());
        this->my_items[this->items.size()] = NULL;

        this->my_menu = new_menu(this->my_items);
        keypad(this->window, TRUE);
        set_menu_win(this->my_menu, this->window);
        set_menu_sub(this->my_menu, derwin(this->window, 6, 38, 3, 1));
        if (this->items.size() > 5)
            set_menu_format(this->my_menu, 5, 1);
        set_menu_mark(this->my_menu, " ->");
        init_border("Menu", numkey);
        post_menu(this->my_menu);
        wrefresh(this->window);
    }

    int func(const char *name) {
        std::string str(name);
        if (str == "Exit")
            return -1;
        if (str == "Menu Exit")
            return 0;
    }
};

#endif //MAIN_MENU_H
