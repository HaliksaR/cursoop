#ifndef MAIN_WINDOWABSTRACT_H
#define MAIN_WINDOWABSTRACT_H

#include <string>

class WindowAbstract {

public:
    virtual void setKey(int) = 0;
    virtual void setHeight(int) = 0;
    virtual void setWidth(int) = 0;
    virtual void setStarty(int) = 0;
    virtual void setStartx(int) = 0;
    virtual void setName(std::string) = 0;

    virtual int getKey() = 0;
    virtual int getHeight() = 0;
    virtual int getWidth() = 0;
    virtual int getStarty() = 0;
    virtual int getStartx() = 0;
    virtual std::string getName() = 0;
};

#endif //MAIN_WINDOWABSTRACT_H
