#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <curses.h>
#include "WindowAbstract.h"

class Window : private WindowAbstract {

private:
    int key;
    int height;
    int width;
    int starty;
    int startx;
    std::string name;
public:
    WINDOW *window;
public:
    Window(int height, int width, int starty, int startx) {
        setHeight(height);
        setWidth(width);
        setStarty(starty);
        setStartx(startx);
        init_win();
    }

    ~Window() { delwin(window); }

    void create_new_win() {
        window = newwin(getHeight(), getWidth(), getStarty(), getStartx());
    }

    void init_border(std::string name, std::string keyName) {
        setName(name);
        box(window, 0, 0);
        print(1, 0, 40, getName(), COLOR_PAIR(1), keyName);
        mvwaddch(window, 2, 0, ACS_LTEE);
        mvwhline(window, 2, 1, ACS_HLINE, getWidth());
        mvwaddch(window, 2, getWidth() - 1, ACS_RTEE);
        wrefresh(window);
        refresh();
    }

    int window_key_events() {
        init_border(getName(), "Esc");
        keypad(this->window, TRUE);
        while (getKey() != 27) {
            setKey(wgetch(this->window));
            switch (getKey()) {
                case KEY_F(1):
                    return 1;
                case KEY_F(2):
                    return 2;
                case KEY_F(3):
                    return 3;
                case KEY_F(4):
                    return 4;
                case KEY_F(5):
                    return 5;
            }
        }
        return 0;
    }

private:
    void setKey(int Key) override {
        this->key = Key;
    }

    void setHeight(int Height) override {
        this->height = Height;
    }

    void setWidth(int Width) override {
        this->width = Width;
    }

    void setStarty(int Starty) override {
        this->starty = Starty;
    }

    void setStartx(int Startx) override {
        this->startx = Startx;
    }

    void setName(std::string Name) override {
        this->name = Name;
    }

    int getKey() override {
        return this->key;
    }

    int getHeight() override {
        return this->height;
    }

    int getWidth() override {
        return this->width;
    }

    int getStarty() override {
        return this->starty;
    }

    int getStartx() override {
        return this->startx;
    }

    std::string getName() override {
        return this->name;
    }

    void init_win() {
        initscr();
        start_color();
        cbreak();
        noecho();
        keypad(stdscr, TRUE);
        init_pair(1, COLOR_RED, COLOR_BLACK);
        init_pair(2, COLOR_GREEN, COLOR_BLACK);
        init_pair(3, COLOR_MAGENTA, COLOR_BLACK);
    }

    void print(int starty, int startx, int width, std::string name, chtype color, std::string keyName) {
        int x, y;
        if (window == NULL) window = stdscr;
        getyx(window, y, x);
        if (startx != 0) x = startx;
        if (starty != 0)y = starty;
        if (width == 0) width = 80;
        name += " : Exit(" + keyName + ")";
        wattron(window, color);
        mvwprintw(window, y, x + 2, "%s", name.c_str());
        wattroff(window, color);
        refresh();
    }
};

#endif //MAIN_WINDOW_H
